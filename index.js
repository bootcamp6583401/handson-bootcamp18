const projectsContainer = document.querySelector("#projects-container");

const projects = projectsContainer.querySelectorAll(".project-child");

const toast = document.querySelector("#toast");

const contactForm = document.querySelector("#contact-form");

const totalProjects = projects.length;

const prevBtn = document.querySelector("#car-prev");
const nextBtn = document.querySelector("#car-next");

const mobileMenu = document.querySelector("#mobile-menu");
const desktopMenu = document.querySelector("#desktop-menu");
const toggle = document.querySelector("#toggle");

toggle.addEventListener("click", () => {
  if (mobileMenu.classList.contains("hidden")) {
    mobileMenu.classList.remove("hidden");
  } else {
    mobileMenu.classList.add("hidden");
  }
});

let currentTranslateVal = 0;

const projectWidth = getPixel(projects[0].style.width);

const incVal = projectWidth + 32;

nextBtn.addEventListener("click", () => {
  if (currentTranslateVal - incVal >= -((totalProjects - 1) * incVal)) {
    currentTranslateVal -= incVal;
    projectsContainer.style.transform = `translateX(${
      currentTranslateVal + "px"
    })`;
  }
});

prevBtn.addEventListener("click", () => {
  if (currentTranslateVal + incVal <= 0) {
    currentTranslateVal += incVal;
    projectsContainer.style.transform = `translateX(${
      currentTranslateVal + "px"
    })`;
  }
});

function getPixel(val) {
  const match = val.match(/(\d+(\.\d+)?)px/);

  if (match) {
    return parseFloat(match[1]);
  } else {
    return 0;
  }
}

contactForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  toast.classList.remove("opacity-0");
  console.log("test");
  toast.textContent = `Message sent.`;

  await new Promise((resolve) => {
    setTimeout(() => {
      toast.classList.add("opacity-0");
      resolve();
    }, 3000);
  });
  toast.classList.remove("transition-all");
});
